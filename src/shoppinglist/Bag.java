package shoppinglist;

public class Bag {
    private String name;
    private int count;
    private double volume;

    public Bag(String name, int count) {
        this.name = name;
        this.count = count;
    }

    public Bag(String name, int count, double volume) {
        this.name = name;
        this.count = count;
        this.volume = volume;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return "shoppingList.Bag{" +
                "name='" + name + '\'' +
                ", count=" + count +
                ", volume=" + volume +
                '}';
    }
}
