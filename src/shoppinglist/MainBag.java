package shoppinglist;

import java.util.ArrayList;
import java.util.List;

public class MainBag {
    public static void main(String[] args) {
        Bag cucumber = new Bag("Cucumber", 3);
        Bag carrot = new Bag("Carrot", 2);
        Bag milk = new Bag("Milk", 1, 2.0);
        Bag coca_cola = new Bag("Coca_cola", 1, 4.0);
        Bag potato = new Bag("Potato", 30);
        List<Bag> bagList = new ArrayList<>();
        bagList.add(carrot);
        bagList.add(cucumber);
        bagList.add(milk);
        bagList.add(coca_cola);
        bagList.add(potato);
        System.out.println(bagList);
    }
}
